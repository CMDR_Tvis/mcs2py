#include "mcs2py.h"

auto newInterpreter(const unsigned short int workersQuantity) -> void * {
  try {
    return new mcs::McsInterpreter(workersQuantity);
  } catch (...) {
  }

  return nullptr;
}

auto deleteInterpreter(void *ptr) -> PyObject * {
  try {
    delete reinterpret_cast<mcs::McsInterpreter *>(ptr);
  } catch (...) {
  }

  return Py_None;
}

auto callPlot(char *expression, const long double fromInclusive,
              const long double toExclusive, const size_t nPoints)
    -> PyObject * {
  try {
    auto result = mcs::McsInterpreter::plot(
        std::string(expression), fromInclusive, toExclusive, nPoints);

    if (result->hasErrors())
      return PyList_FromStringVector(result->errorsOrNull());

    return PyDict_FromLongDoubleLongDoubleMap(result->valueOrNull());
  } catch (...) {
    return Py_None;
  }
}

auto callHistogram(void *ptr, const char *expression,
                   const unsigned long long nValues, const long double xMin,
                   const long double xMax, const long double histogramMin,
                   const long double histogramMax, const size_t nBins)
    -> PyObject * {
  try {
    auto result = (reinterpret_cast<mcs::McsInterpreter *>(ptr))
                      ->histogram(std::string(expression), nValues, xMin, xMax,
                                  histogramMin, histogramMax, nBins);

    if (result->hasErrors())
      return PyList_FromStringVector(result->errorsOrNull());

    return PyList_FromLongDoubleVector(result->valueOrNull());
  } catch (...) {
    return Py_None;
  }
}

auto PyList_FromStringVector(std::vector<std::string> *source) -> PyObject * {
  auto pyList = PyList_New(source->size());
  auto idx = static_cast<size_t>(0ul);

  for (auto it = source->begin(); it != source->end(); ++it, ++idx)
    PyList_SetItem(pyList, idx, PyUnicode_FromString((*it).c_str()));

  return pyList;
}

auto PyFloat_FromLongDouble(long double source) -> PyObject * {
  return PyFloat_FromDouble(static_cast<double>(source));
}

auto PyDict_FromLongDoubleLongDoubleMap(
    std::map<long double, long double> *source) -> PyObject * {
  auto resultDict = PyDict_New();

  std::for_each(source->begin(), source->end(),
                [resultDict](std::pair<long double, long double> p) {
                  PyDict_SetItem(resultDict, PyFloat_FromDouble(p.first),
                                 PyFloat_FromDouble(p.second));
                });

  return resultDict;
}

auto PyList_FromLongDoubleVector(std::vector<long double> *source)
    -> PyObject * {
  auto pyList = PyList_New(source->size());
  auto idx = static_cast<size_t>(0ul);

  for (auto it = source->begin(); it != source->end(); ++it, ++idx)
    PyList_SetItem(pyList, idx, PyFloat_FromLongDouble(*it));

  return pyList;
}

auto callEvaluate(const char *expression, const long double x) -> PyObject * {
  try {
    auto result = mcs::McsInterpreter::evaluate(expression, x);

    if (result->hasErrors())
      return PyList_FromStringVector(result->errorsOrNull());

    return PyFloat_FromLongDouble(*result->valueOrNull());
  } catch (...) {
    return Py_None;
  }
}
