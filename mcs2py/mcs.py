from ctypes import CDLL, c_void_p, c_ushort, c_ulonglong, py_object, c_char_p, c_longdouble, c_size_t
from typing import List, Dict, Optional
from requests import get
from pathlib import Path

_mcs_path = Path.home() / '.mcs'
_mcs_path.mkdir(parents=True, exist_ok=True)
_libmcs2py_path = _mcs_path / 'libmcs2py.so'

if not _libmcs2py_path.exists():
    _LIBRARY_PATH = 'https://gitlab.com/CMDR_Tvis/mcs2py/-/jobs/artifacts/master/raw/cmake-build-release/libmcs2py.so' \
                    '?job=build:gcc'

    print(f'Loading natives from {_LIBRARY_PATH}...')
    _data = get(_LIBRARY_PATH).content
    _out = open(str(_libmcs2py_path), 'wb')
    _out.write(_data)

_libmcs2py: CDLL = CDLL(str(_libmcs2py_path))

# auto newInterpreter(unsigned short int workersQuantity) -> void *;
_libmcs2py.newInterpreter.restype = c_void_p
_libmcs2py.newInterpreter.argtypes = [c_ushort]

# auto deleteInterpreter(void *ptr) -> PyObject *;
_libmcs2py.deleteInterpreter.restype = py_object
_libmcs2py.deleteInterpreter.argtypes = [c_void_p]

# auto callPlot(char *expression, long double fromInclusive,
#               long double toExclusive, size_t nPoints) -> PyObject *;
_libmcs2py.callPlot.restype = py_object
_libmcs2py.callPlot.argtypes = [c_char_p, c_longdouble, c_longdouble, c_size_t]

# auto callHistogram(void *ptr, const char *expression,
#                    unsigned long long nValues, long double xMin,
#                    long double xMax, long double histogramMin,
#                    long double histogramMax, size_t nBins) -> PyObject *;
_libmcs2py.callHistogram.restype = py_object
_libmcs2py.callHistogram.argtypes = [c_void_p, c_char_p, c_ulonglong, c_longdouble, c_longdouble, c_longdouble,
                                     c_longdouble, c_size_t]

# auto callEvaluate(const char *expression, long double x) -> PyObject *;
_libmcs2py.callEvaluate.restype = py_object
_libmcs2py.callEvaluate.argtypes = [c_char_p, c_longdouble]


class McsInterpreter(object):
    def __init__(self, workers_quantity: int) -> None:
        self.ptr: c_void_p = _libmcs2py.newInterpreter(c_ushort(workers_quantity))

    def __del__(self) -> None:
        return _libmcs2py.deleteInterpreter(self.ptr)

    @staticmethod
    def plot(expression: str, from_inclusive: float, to_exclusive: float, n_points: int) \
            -> Union[Dict[float, float], List[str], None]:
        return _libmcs2py.callPlot(c_char_p(expression.encode()), c_longdouble(from_inclusive),
                                   c_longdouble(to_exclusive), c_size_t(n_points))

    def histogram(self, expression: str, n_values: int, x_min: float, x_max: float, histogram_min: float,
                  histogram_max: float, n_bins: int) -> Union[List[str], List[float], None]:
        return _libmcs2py.callHistogram(self.ptr, c_char_p(expression.encode()), c_ulonglong(n_values),
                                        c_longdouble(x_min), c_longdouble(x_max), c_longdouble(histogram_min),
                                        c_longdouble(histogram_max), c_size_t(n_bins))

    @staticmethod
    def evaluate(expression: str, x: float) -> Union[List[str], float, None]:
        return _libmcs2py.callEvaluate(c_char_p(expression.encode()), c_longdouble(x))
