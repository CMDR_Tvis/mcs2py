from tkinter import Tk, Button, Entry, StringVar, Label, LabelFrame
from typing import Dict, List, Optional

from matplotlib.axes import Axes
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from matplotlib.pyplot import ion, figure

from mcs import McsInterpreter


def remove_prefix(text: str, prefix: str) -> str:
    return text[text.startswith(prefix) and len(prefix):]


class McsUI(LabelFrame):
    __interpreter: McsInterpreter = McsInterpreter(workers_quantity=4)

    def __init__(self, master: Tk) -> None:
        LabelFrame.__init__(self, master, text="Monte-Carlo Script")
        self.hist_formula = StringVar(master=self, value="x")
        self.plot_formula: StringVar = StringVar(master=self, value="1")
        self.plot_min_x: StringVar = StringVar(master=self, value="0.0")
        self.plot_max_x: StringVar = StringVar(master=self, value="1.0")
        self.hist_min_x: StringVar = StringVar(master=self, value="0.0")
        self.hist_max_x: StringVar = StringVar(master=self, value="1.0")
        self.hist_min_y: StringVar = StringVar(master=self, value="0.0")
        self.hist_max_y: StringVar = StringVar(master=self, value="1.0")
        self.hist_n_points: StringVar = StringVar(master=self, value="1000000")
        self.hist_n_bins: StringVar = StringVar(master=self, value="10")
        self.plot_n_points: StringVar = StringVar(master=self, value="1000")
        self.canvas: Optional[FigureCanvasTkAgg] = None
        self.figure: Optional[Figure] = None
        self.plot_subplot: Optional[Axes] = None
        self.hist_subplot: Optional[Axes] = None
        self.master.wm_title("Monte-Carlo Script")
        self.draw_controls()
        self.draw_canvas()

    def draw_canvas(self) -> None:
        self.figure: Figure = figure(figsize=(6, 3), dpi=100)
        (self.plot_subplot, self.hist_subplot) = self.figure.subplots(ncols=2)
        self.__create_plot()
        self.__create_hist()
        self.canvas = FigureCanvasTkAgg(self.figure, master=self)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row=4, column=0, in_=self, columnspan=6, rowspan=2)

    def __create_plot(self):
        points: Dict[float, float] = McsUI.__interpreter.plot(self.plot_formula.get(), float(self.plot_min_x.get()),
                                                              float(self.plot_max_x.get()),
                                                              int(self.plot_n_points.get()))

        self.plot_subplot.plot([k for k in points], [k for k in points.values()])

    def __quit_ui(self):
        self.quit()
        self.destroy()

    def draw_controls(self):
        def calculate():
            self.plot_subplot.clear()
            self.hist_subplot.clear()
            self.__create_plot()
            self.__create_hist()
            self.canvas.draw()

        plot_formula_label: Label = Label(master=self, text="Plot formula: ")
        plot_formula_label.grid(row=0, column=0)
        plot_formula_input: Entry = Entry(master=self, textvariable=self.plot_formula)
        plot_formula_input.grid(row=0, column=1)

        hist_formula_label: Label = Label(master=self, text="Histogram formula: ")
        hist_formula_label.grid(row=0, column=2)
        hist_formula_input: Entry = Entry(master=self, textvariable=self.hist_formula)
        hist_formula_input.grid(row=0, column=3)

        calculate_button: Button = Button(master=self, text="Calculate", command=calculate)
        calculate_button.grid(row=0, column=4)

        quit_button: Button = Button(master=self, text="Quit", command=self.__quit_ui)
        quit_button.grid(row=0, column=5)

        plot_min_x_label: Label = Label(master=self, text="Minimal x of plot: ")
        plot_min_x_label.grid(row=1, column=0)
        plot_min_x_input = Entry(master=self, textvariable=self.plot_min_x)
        plot_min_x_input.grid(row=1, column=1)

        plot_max_x_label = Label(master=self, text="Maximal x of plot: ")
        plot_max_x_label.grid(row=1, column=2)
        plot_max_x_input = Entry(master=self, textvariable=self.plot_max_x)
        plot_max_x_input.grid(row=1, column=3)

        plot_max_x_label = Label(master=self, text="Quantity of points on plot: ")
        plot_max_x_label.grid(row=1, column=4)
        plot_max_x_input = Entry(master=self, textvariable=self.plot_n_points)
        plot_max_x_input.grid(row=1, column=5)

        hist_min_x_label: Label = Label(master=self, text="Minimal x of histogram: ")
        hist_min_x_label.grid(row=2, column=0)
        hist_min_x_input = Entry(master=self, textvariable=self.hist_min_x)
        hist_min_x_input.grid(row=2, column=1)

        hist_max_x_label = Label(master=self, text="Maximal x of histogram: ")
        hist_max_x_label.grid(row=2, column=2)
        hist_max_x_input = Entry(master=self, textvariable=self.hist_max_x)
        hist_max_x_input.grid(row=2, column=3)

        hist_max_x_label = Label(master=self, text="Quantity of points for histogram: ")
        hist_max_x_label.grid(row=2, column=4)
        hist_max_x_input = Entry(master=self, textvariable=self.hist_n_points)
        hist_max_x_input.grid(row=2, column=5)

        hist_min_x_label: Label = Label(master=self, text="Minimal y of histogram: ")
        hist_min_x_label.grid(row=3, column=0)
        hist_min_x_input = Entry(master=self, textvariable=self.hist_min_y)
        hist_min_x_input.grid(row=3, column=1)

        hist_max_x_label = Label(master=self, text="Maximal y of histogram: ")
        hist_max_x_label.grid(row=3, column=2)
        hist_max_x_input = Entry(master=self, textvariable=self.hist_max_y)
        hist_max_x_input.grid(row=3, column=3)

        hist_max_x_label = Label(master=self, text="Quantity of bins of histogram: ")
        hist_max_x_label.grid(row=3, column=4)
        hist_max_x_input = Entry(master=self, textvariable=self.hist_n_bins)
        hist_max_x_input.grid(row=3, column=5)

    def __create_hist(self):
        heights: List[float] = McsUI.__interpreter.histogram(self.hist_formula.get(), int(self.hist_n_points.get()),
                                                             float(self.hist_min_x.get()),
                                                             float(self.hist_max_x.get()), float(self.hist_min_y.get()),
                                                             float(self.hist_max_y.get()),
                                                             int(self.hist_n_bins.get()))

        increment = (float(self.hist_max_y.get()) - float(self.hist_min_y.get())) / int(self.hist_n_bins.get())

        for i, k in enumerate(heights):
            heights[i] = heights[i] / increment

        x: List[float] = [float(self.hist_min_y.get())]

        i = 1

        while i < float(self.hist_n_bins.get()):
            x.append(x[i - 1] + increment)
            i += 1

        assert len(x) == len(heights)
        self.hist_subplot.bar(x=x, height=heights, width=increment, align='center')


root: Tk = Tk()
app = McsUI(master=root)
app.pack()
root.mainloop()
