#ifndef MCS2PY_MCS2PY_H
#define MCS2PY_MCS2PY_H
#include <McsInterpreter.hpp>
#include <Python.h>
#include <new>
#include <object.h>
#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

auto newInterpreter(unsigned short int workersQuantity) -> void *;

auto deleteInterpreter(void *ptr) -> PyObject *;

auto callPlot(char *expression, long double fromInclusive,
              long double toExclusive, size_t nPoints) -> PyObject *;

auto callHistogram(void *ptr, const char *expression,
                   unsigned long long nValues, long double xMin,
                   long double xMax, long double histogramMin,
                   long double histogramMax, size_t nBins) -> PyObject *;

auto callEvaluate(const char *expression, long double x) -> PyObject *;

#ifdef __cplusplus
}
#endif // __cplusplus

auto PyList_FromStringVector(std::vector<std::string> *source) -> PyObject *;

auto PyList_FromLongDoubleVector(std::vector<long double> *source)
    -> PyObject *;

auto PyDict_FromLongDoubleLongDoubleMap(
    std::map<long double, long double> *source) -> PyObject *;

auto PyFloat_FromLongDouble(long double source) -> PyObject *;

#endif // MCS2PY_MCS2PY_H
